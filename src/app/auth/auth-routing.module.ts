import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AuthComponent} from "./auth.component";


const routes: Routes = [
  {
    path: "", component: AuthComponent, children: [

    {
      path: "step-2",
      loadChildren: "./registration/register-step-1/register-step-1.module#RegisterStep1Module"
    },
    {
      path: "step-1",
      loadChildren: "./registration/register/register.module#RegisterModule"
    },
  ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})

export class AuthRoutingModule {
}

